# Beer-fests.com
[www.beer-fests.com](https://www.beer-fests.com)

## What is it?

Did you ever visited a beer festival like [van moll fest](http://vanmollcraftbeer.com/event/van-moll-festival/)? All these beer manufacturers together, with these fancy little stands where they can give you a taste of their latest and greatest beers.
Finally you can drink your favourite beers from breweries like [GarageBeer](https://www.garagebeer.co/) and [Põhjala](https://pohjalabeer.com/) drafted.. Imagine, a Garage Double Soup or a Põhjala Öö XO Cognac Balled Aged Imperial Baltic Porter from a beer tap..
You're preparing yourself to this nice festival, reading everything there is to know about the beers on [Untappd](https://untappd.com) and you made a plan which beers you want to drink..

But what usually happens? Each brewer has 10 different beers and they on have 3 taps in their stand? So I order to drink what you've planned to drink, you have to constantly visit each stand to look what's on the taps. And of course, you will miss this sweet spot when the Double Soup was available...

*This should never happen to any beer lover again!*

This is where Beer-fests.com comes to play!

## How does it work?

Of course there's an app. This app has information about the beerfestivals. It includes location, layout, breweries, beers, type of beers and so on. Before the festival starts, you can already favorite your beers, so you won't miss them.

Once the festival is on, the app will show which beer is currently available at which stand. 

This is either done by a) the brewer when he or she changes barrels, b) a beerdrinker who visits the booth and sees what's available or c) depending on untappd checkins guess what's on. 

## How does it look?

[![Beer-fests.com mockup](http://img.youtube.com/vi/Jh1P-Qs-uM8/0.jpg)](https://www.youtube.com/watch?v=Jh1P-Qs-uM8 "Beer-fests.com mockup")

## Who?

- Niek Palm [@dev.npalm](https://gitlab.com/dev.npalm)
- Jeroen Knoops [@JeroenKnoops](https://gitlab.com/JeroenKnoops)

## License

[MIT license](./LICENSE)
