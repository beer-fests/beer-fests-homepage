import React, { Component } from "react";
import { Pane, Strong, Heading, Paragraph, Link } from "evergreen-ui";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Pane width={1024} height={500} border="default">
            <Heading size={800}>Beer-fests.com</Heading>
            <Heading size={600} marginTop={20}>
              What is it......?
            </Heading>
            <Paragraph>
              Did you ever visited a beer festival like{" "}
              <Link href="http://vanmollcraftbeer.com/event/van-moll-festival/">
                van moll fest
              </Link>
              ? All these beer manufacturers together, with these fancy little
              stands where they can give you a taste of their latest and
              greatest beers. Finally you can drink your favourite beers from
              breweries like{" "}
              <Link href="https://www.garagebeer.co">GarageBeer</Link> and{" "}
              <Link href="https://pohjalabeer.com">Põhjala</Link> drafted..
              Imagine, a Garage Double Soup or a Põhjala Öö XO Cognac Balled
              Aged Imperial Baltic Porter from a beer tap.. You're preparing
              yourself to this nice festival, reading everything there is to
              know about the beers on{" "}
              <Link href="https://untappd.com">Untappd</Link> and you made a
              plan which beers you want to drink.. But what usually happens?
              Each brewer has 10 different beers and they on have 3 taps in
              their stand? So I order to drink what you've planned to drink, you
              have to constantly visit each stand to look what's on the taps.
              And of course, you will miss this sweet spot when the Double Soup
              was available...
              <Strong>This should never happen to any beer lover again!</Strong>
              This is where Beer-fests.com comes to play!
            </Paragraph>
            <Heading size={600} marginTop={20}>
              How does it work?
            </Heading>
            <Paragraph>
              Of course there's an app. This app has information about the
              beerfestivals. It includes location, layout, breweries, beers,
              type of beers and so on. Before the festival starts, you can
              already favorite your beers, so you won't miss them. Once the
              festival is on, the app will show which beer is currently
              available at which stand. This is either done by a) the brewer
              when he or she changes barrels, b) a beerdrinker who visits the
              booth and sees what's available or c) depending on untappd
              checkins guess what's on.
            </Paragraph>
            <Heading size={600} marginTop={20}>
              How does it look?
            </Heading>
            <Paragraph>
              <Link href="https://www.youtube.com/watch?v=Jh1P-Qs-uM8">
                Movie mockup app
              </Link>
            </Paragraph>
          </Pane>
        </header>
      </div>
    );
  }
}

export default App;
